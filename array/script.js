/* array baru yang berisikan tiap tiap kata dari nama lengkap. */
var nama = ["Abdurraziq", "Bachmid"];

/* array kedua yang berisikan umur */
var umur = [28];

/* Menggabungkan kedua array diatas menggunakan concat(). */
var gabung = nama.concat(umur);
console.log(gabung);


/* Menyelipkan tahun kelahiran di antara nama dan umur */
gabung.splice(2, 0, 1992);

for (var i = 0; i < gabung.length; i++) {
    console.log(gabung[i]);
}
